APP_ENTRY=org.tio.http.apitool.HttpApiToolStarter
BASE=$(pwd)
CP=${BASE}/config:${BASE}/lib/*
java -Xverify:none -Xms128m -Xmx1024m -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/java-tio-pid.hprof -cp ${CP} ${APP_ENTRY}