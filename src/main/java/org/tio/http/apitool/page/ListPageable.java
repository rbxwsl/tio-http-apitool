package org.tio.http.apitool.page;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author tanyaowu
 * @param <T>
 */
public class ListPageable<T> extends AbstractPageable {
	@SuppressWarnings("unused")
	private static Logger log = LoggerFactory.getLogger(ListPageable.class);

	/**
	 * @param args
	 */
	public static void main(String[] args) {
	}

	private List<T> allData = null;

	public ListPageable() {
	}

	/**
	 * 
	 * @param pageSize
	 * @param pageIndex
	 * @param allData
	 */
	public ListPageable(int _pageSize, int _pageIndex, List<T> _allData) {
		this.pageSize = processPageSize(_pageSize);
		this.pageIndex = processPageIndex(_pageIndex);

		this.allData = _allData;

		if (_allData != null) {
			this.recordCount = _allData.size();
		} else {
			this.recordCount = 0;
		}

		if (this.isPagination) {
			long pageCount = calculatePageCount(this.pageSize, recordCount);
			if (this.pageIndex > pageCount) {
				this.pageIndex = pageCount;
			}
			if (this.pageIndex <= 0) {
				this.pageIndex = 1;
			}

			List<Object> data1 = new ArrayList<Object>(this.pageSize);
			int startIndex = (int) (this.pageIndex - 1) * this.pageSize;
			int endIndex = (int) (this.pageIndex) * this.pageSize;

			if (this.allData == null) {
				endIndex = 0;
			} else if (endIndex > allData.size()) {
				endIndex = allData.size();
			}

			for (int i = startIndex; i < endIndex; i++) {
				data1.add(allData.get(i));
			}
			this.data = data1;
		} else {
			this.data = allData;
		}
	}

	/**
	 * 
	 * @param pageSize
	 * @param pageIndex
	 * @param recordCount
	 * @param currentPageData
	 */
	public ListPageable(int pageSize, int pageIndex, long recordCount, List<T> currentPageData) {
		this.pageSize = processPageSize(pageSize);
		this.pageIndex = processPageIndex(pageIndex);
		this.recordCount = recordCount;
		this.data = currentPageData;
	}

	@Override
	public void reload() {
		//		final List<Integer> piDigits = {3, 1, 4, 1, 5, 9, 2, 6, 5, 3, 5, 9};
	}

}
