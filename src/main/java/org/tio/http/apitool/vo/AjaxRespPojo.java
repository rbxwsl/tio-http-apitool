/**
 * 
 */
package org.tio.http.apitool.vo;

/**
 * 
 * 
 * @filename:	 com.talent.platform.core.vo.AjaxRespPojo
 * @copyright:   Copyright (c)2010
 * @company:     talent
 * @author:      谭耀武
 * @version:     1.0
 * @create time: 2013-5-18 下午2:12:26
 * @record
 * <table cellPadding="3" cellSpacing="0" style="width:600px">
 * <thead style="font-weight:bold;background-color:#e3e197">
 * 	<tr>   <td>date</td>	<td>author</td>		<td>version</td>	<td>description</td></tr>
 * </thead>
 * <tbody style="background-color:#ffffeb">
 * 	<tr><td>2013-5-18</td>	<td>谭耀武</td>	<td>1.0</td>	<td>create</td></tr>
 * </tbody>
 * </table>
 */
public class AjaxRespPojo
{

	/**
	 * 失败的result
	 */
	public static final Short RESULT_CODE_FAIL = 0;

	/**
	 * 成功的result
	 */
	public static final Short RESULT_CODE_SUCCESS = 1;



	/**
	 * 
	 */
	private Object data;

	/**
	 * 
	 */
	private String msg;

	/**
	 * 0表示成功, 1表示失败
	 */
	private Short result;

	/**
	 * 业务错误码
	 */
	private Integer code;

	/**
	 * 
	 */
	private String title;

	/**
	 * 
	 */
	public AjaxRespPojo()
	{
	}

	public AjaxRespPojo(Short result, String title, String msg)
	{
		super();
		this.result = result;
		this.title = title;
		this.msg = msg;
	}

	public AjaxRespPojo(Short result, String title, String msg, Object data)
	{
		this(result, null, title, msg, data);
	}

	public AjaxRespPojo(Short result, Integer code, String title, String msg, Object data)
	{
		super();
		this.result = result;
		this.title = title;
		this.msg = msg;
		this.data = data;
		this.code = code;
	}

	public Object getData()
	{
		return data;
	}

	public String getMsg()
	{
		return msg;
	}

	public Short getResult()
	{
		return result;
	}

	public String getTitle()
	{
		return title;
	}

	public void setData(Object data)
	{
		this.data = data;
	}

	public void setMsg(String msg)
	{
		this.msg = msg;
	}

	public void setResult(Short result)
	{
		this.result = result;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public Integer getCode()
	{
		return code;
	}

	public void setCode(Integer code)
	{
		this.code = code;
	}

}
