/**
 *
 */
package org.tio.http.apitool.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tio.http.apitool.vo.AjaxRespPojo;
import org.tio.http.common.HttpRequest;

/**
 * 
 * @author tanyaowu
 */
public class WebUtils {
	public static final String HEADER_SKIPCACHEPROCESS = "ttskipcacheprocess";

	/**
	 * 用于记录本次请求给客户端是成功的响应还是失败的响应
	 * 1：成功，2：失败
	 */
	public static final String ATT_RESPONSE_RESULT = "ATT_RESPONSE_RESULT";

	private static Logger log = LoggerFactory.getLogger(WebUtils.class);

	/**
	 * 
	 * @param request
	 * @return
	 */
	public static boolean isAjaxRequest(HttpRequest request) {
		String requestedWith = request.getHeader("X-Requested-With");
		return requestedWith != null && "XMLHttpRequest".equals(requestedWith);
	}

	/**
	 * 
	 * @param result
	 * @param title
	 * @param msg
	 * @param data
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public static AjaxRespPojo writeVo(Integer result, String title, String msg, Object data, HttpRequest request) throws Exception {
		return writeVo(result.shortValue(), null, title, msg, data, request);
	}

	/**
	 * 
	 * @param result
	 * @param code
	 * @param title
	 * @param msg
	 * @param data
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public static AjaxRespPojo writeVo(Short result, Integer code, String title, String msg, Object data, HttpRequest request) throws Exception {
		return new AjaxRespPojo(result, code, title, msg, data);
	}

	/**
	 * 
	 * @param code
	 * @param title
	 * @param msg
	 * @param data
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public static AjaxRespPojo writeFail(Integer code, String title, String msg, Object data, HttpRequest request) throws Exception {
		log.error("msg:{}, title:{}, data:{}", msg, title, data);
		request.setAttribute(ATT_RESPONSE_RESULT, AjaxRespPojo.RESULT_CODE_FAIL);
		return writeVo(AjaxRespPojo.RESULT_CODE_FAIL, code, title, msg, data, request);
	}

	/**
	 * 
	 * @param title
	 * @param msg
	 * @param data
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public static AjaxRespPojo writeFail(String title, String msg, Object data, HttpRequest request) throws Exception {
		return writeFail(null, title, msg, data, request);
	}

	/**
	 * 
	 * @param title
	 * @param msg
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public static AjaxRespPojo writeFail(String title, String msg, HttpRequest request) throws Exception {
		return writeFail(title, msg, null, request);
	}

	public static AjaxRespPojo writeFail(Exception e, HttpRequest request) throws Exception {
		return writeFail("服务器异常", "服务器异常", null, request);
	}

	/**
	 * 
	 * @param title
	 * @param msg
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public static AjaxRespPojo writeSuccess(String title, String msg, HttpRequest request) throws Exception {
		return writeSuccess(title, msg, null, request);
	}

	/**
	 * 
	 * @param title
	 * @param msg
	 * @param data
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public static AjaxRespPojo writeSuccess(String title, String msg, Object data, HttpRequest request) throws Exception {
		return writeSuccess(null, title, msg, data, request);
	}

	/**
	 * 
	 * @param code
	 * @param title
	 * @param msg
	 * @param data
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public static AjaxRespPojo writeSuccess(Integer code, String title, String msg, Object data, HttpRequest request) throws Exception {
		request.setAttribute(ATT_RESPONSE_RESULT, AjaxRespPojo.RESULT_CODE_SUCCESS);
		return writeVo(AjaxRespPojo.RESULT_CODE_SUCCESS, code, title, msg, data, request);
	}

}
